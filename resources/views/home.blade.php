@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 general col-md-offset-2">
                @if (Auth::guest())
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>
                        <div class="panel-body">
                            Please login to your FB account for reading your newsfeed
                        </div>
                    </div>
            @else
                <ul class="media-list content-list">
                    {{--@include('_common._posts')--}}
                </ul>
                    <button class="btn btn-block btn-more btn-default" onclick="getAnother()">Еще новости</button>
            @endif
            </div>
            <div class="col-0 additional"></div>
        </div>
    </div>
@endsection
