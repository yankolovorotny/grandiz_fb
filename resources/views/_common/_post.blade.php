    <?php
    $item = \GuzzleHttp\json_decode($post->json);
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <li class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="http://graph.facebook.com/{{$item->from->id}}/picture" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{$item->from->name}}</h4>
                        <?
                        switch($item->type)
                        {
                            case 'link':
                                echo(!empty($item->story))?$item->story:'Published a post';//$item->message;
                                break;
                            case "photo":
                                echo(!empty($item->story))?$item->story:(!empty($item->message)?$item->message:'Shared a photo');
                                break;
                            case 'video':
                                echo(!empty($item->story))?$item->story:(!empty($item->description)?$item->description:$item->message);
                                break;
                            case 'status':
                                echo(!empty($item->story))?$item->story:$item->message;
                                break;
                        }
                        ?>
                        <div class="col-md-12">
                            <img class="img-responsive" src="{{ $item->picture or '' }}"/>
                        </div>
                        <div class="col-md-12">
                            @if(!property_exists($item, 'likes'))
                                    No one likes it
                            @else
                                    Liked:
                            @foreach($item->likes->data as $likes)
                                    <img class="img-circle" src="http://graph.facebook.com/{{$likes->id}}/picture"/>
                            @endforeach
                            @endif
                        </div>
                </div>
            </li>
        </div>
    </div>
