@if( ! $posts->isEmpty() )
    @foreach($posts as $post)
        <?php
        $item = \GuzzleHttp\json_decode($post->json);
        ?>
        <div class="panel panel-default item"  data-id="{{$post->id}}">
            <div class="panel-body">
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" src="http://graph.facebook.com/{{$item->from->id}}/picture" alt="...">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">{{$item->from->name}}</h4>
                        <a href="#!" onclick="getDetails(this)" data-id="{{$item->id}}">
                            <?
                                switch($item->type)
                                {
                                case 'link':
                                    echo(!empty($item->story))?$item->story:(!empty($item->message)?$item->message:'Shared link');
                                break;
                                case "photo":
                                    echo(!empty($item->story))?$item->story:(!empty($item->message)?substr(substr($item->message, 0, 100), 0, strrpos(substr($item->message, 0, 100), ' ')).'...':'Shared a photo');
                                break;
                                case 'video':
                                    echo(!empty($item->story))?$item->story:(!empty($item->description)?$item->description:$item->message);
                                break;
                                case 'status':
                                    echo(!empty($item->story))?$item->story:(!empty($item->message)?$item->message:'Updated status');
                                break;
                                }
                            ?>
                        </a>
                    </div>
                </li>
            </div>
        </div>
        <script>
            $('.btn-more').data('id',$('.item').last().data('id'));
        </script>
    @endforeach
@endif
