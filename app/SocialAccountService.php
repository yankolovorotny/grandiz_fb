<?php

namespace App;
use App\NewsFeed;
use Laravel\Socialite\Contracts\User as ProviderUser;
class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = Profile::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
//            $this->dumpNewsFeed($providerUser->getId(),$providerUser->token);
            return $account->user;
        } else {

            $account = new Profile([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();
            //dd($providerUser);
            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'avatar' => $providerUser->getAvatar(),
                    'name' => $providerUser->getName(),
                    'remember_token' => $providerUser->token,
                ]);
            }

            $account->user()->associate($user);
            $account->save();

//            $this->dumpNewsFeed($providerUser->getId(),$providerUser->token);
            return $user;

        }

    }
//    private function dumpNewsFeed($uid,$token)
//    {
//        //TODO:change static token to $token when FB APP is a proven
//        $client = new \GuzzleHttp\Client();
//        $res = $client->request('GET', 'https://graph.facebook.com/v2.2/me/home?limit=100&access_token=EAACEdEose0cBAEml48zJWjR5S3RKhphEvn3JFuPqa8rbsl4hG3wZCkbKxl11AxZC04narZC0BqoFt75qEYkJGitN1dmZARqP6wdOZAZCMPBHxF5LQ1R6p1a5iCP2VnZBu68ashsDLkfoR8yW4Cma0Jz7AZC8ARLBu3ZAZAvWMLZCZCHEWwZDZD');
//        $response =  \GuzzleHttp\json_decode($res->getBody());
//        foreach ($response->data as $data)
//        {
//            NewsFeed::where('mid', $data->id)->delete();
//            NewsFeed::Create(['uid' => $uid, 'mid' => $data->id, 'json' => json_encode($data)]);
//        }
//    }
}