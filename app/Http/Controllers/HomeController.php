<?php

namespace App\Http\Controllers;

use App\NewsFeed;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {
        $posts = NewsFeed::where('uid', Auth::user()->id)->take(2)->get();
        return view('_common._posts', compact('posts'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function display()
    {
        if(!Auth::guest())
            $this->dumpNewsFeed(Auth::user()->id,Auth::user()->remember_token);
        return view('home');
    }

    private function dumpNewsFeed($uid,$token)
    {
        //TODO:change static token to $token when FB APP is a proven
        $client = new Client();
        $res = $client->request('GET', 'https://graph.facebook.com/v2.2/me/home?limit=100&access_token=EAACEdEose0cBAIaMKd4fJ8Gi2BZC4d4nEKEdJbpSOw28DDzNnbSRRoYmsZBwG91ERWxedtaxctl7yZAYJSH03sQfvgCyI2uUVYIG8zxmwJzVkYZBeoETqe0eDxJByZBTF5pX9Rs4tlikmMv5ffJVskZA4ghPmSoxDj6yZCobzB2pnDOFtuY2xPM');
        $response =  \GuzzleHttp\json_decode($res->getBody());
        foreach ($response->data as $data)
        {
            NewsFeed::where('mid', $data->id)->delete();
            NewsFeed::Create(['uid' => $uid, 'mid' => $data->id, 'json' => json_encode($data)]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = NewsFeed::where('mid',$id)
            ->where('uid', Auth::user()->id)
            ->first();
        return view('_common._post', compact('post'));
    }

    public function more($id)
    {
        $posts = NewsFeed::where('uid', Auth::user()->id)
            ->where('id', '>', $id)
            ->orderBy('id', 'asc')
            ->take(2)->get();
        return view('_common._posts', compact('posts'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
