$.get('/api', function(data){
    $('.content-list').html(data);
});
function getDetails(obj) {
    $('.additional').removeClass('col-0');
    $('.general').removeClass('col-md-8');
    $('.general').removeClass('col-md-offset-2');
    $('.additional').addClass('col-md-8');
    $('.general').addClass('col-md-4');
    $.get('/api/'+obj.dataset.id, function(data){
        $('.additional').html(data);
    });
}
function getAnother()
{
    $.get('/api/more/'+$('.btn-more').data('id'), function(data){
        $('.content-list').append(data);
    });
}